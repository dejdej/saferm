## Safe Rm

### Conceived from the single reason that `rm` should not be trusted in the hands of the tired and weary.

#### _What_ it should do

- be an in-place replacement to the classic `rm`, but...
- should create back-ups of the deleted items, in case of an erroneous operation
    - backups should be per mount point (Linux specific I guess?)
    - account for files that have been already backed-up
- should allow certain exceptions that can be safely assumed as always removable
    - files from temporary file systems
    - cache files
    - special devices?
    - files deleted by trusted users/groups or processes
- list all the existing backups
    - probably the _backend_ should only work with IDs, and paths, and the like
     and the _frontend_ converts this information in something user-friendly
- should allow the user in a friendly manner to restore
    - using the _backend_ (which shall probably be just a library) we could create
     a kind of terminal user interface with interactive selection and stuff like that
- use a sane default configuration, but also allow the user to change its behaviour

#### _How_ it should do it

##### Library 

- module for configuration
    - ~~should define the structure~~
    - ~~provide sane defaults~~
    - ~~number of backups to keep~~
    - ~~path to metadata~~
    - ~~path to actual backups~~
    - ~~exclude tmpfs and the like~~
    - excluded PIDs -- this seems rather useless
    - excluded UIDs
    - excluded GIDs
    - excluded process path? -- risky
        - let's say all `/sbin`, `/usr/sbin/`, `/usr/local/sbin` are trusted
         and stuff like that
- module for backup metadata
    - per-mount, or per-user?
        - if the user can create the meta file in the mount path it should be done,
         otherwise create in XDG_DATA or something
            - `<mount-root>/.saferm/meta`, `$XDG_DATA_HOME/saferm/meta`, `$HOME/.saferm/meta`
    - ~~ID~~
    - ~~path~~
    - ~~original path~~
    - PID
    - UID (this isn't possible in all cases and it is OS specific)
    - GID (this isn't possible in all cases and it is OS specific)
    - process name (this isn't possible in all cases and it is OS specific)
- module for the actual `backend`
    - constructor based on configuration module
    - load metadata(backup) config
    - sanity checks - can I ...?
        - delete
        - move
        - create
    - delete - backup
        - check if there are already existing backups
        - create backup and update database(json?) with metadata
    - list
        - return a list of backups (ID, original path, backup date, user?, command that backed-up?)
    - restore
        - takes a list of IDs
        - **if there's already a file at restore path, restore with timestamp appended**
            - should this be user configurable?
        - if the restore isn't successful, keep the backup
    - delete - erase
        - update metadata
        - remove backup
        - **what if erase fails?**
    

##### User application

- ??
- probably a fancy TUI application for list/restore that permits interactive selection
- delete(backup) should be a drop-in for POSIX `rm` 
