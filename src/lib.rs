#[macro_use]
extern crate log;

#[macro_use]
extern crate serde;

extern crate directories;
extern crate fs_extra;
extern crate serde_yaml;
extern crate sysinfo;
extern crate tempfile;
extern crate users;

pub mod srm;
