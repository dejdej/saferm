use std::{
    fs::{self, OpenOptions},
    path::{Path, PathBuf},
};

use directories::BaseDirs;
use tempfile::tempfile_in;

use crate::srm::{
    config::{
        error::{Error, Result},
        Config,
    },
    utils::path::MountUtils,
};

#[derive(Deserialize, Debug, Default)]
struct CfgFile {
    count: Option<usize>,
    home: Option<PathBuf>,
    meta: Option<PathBuf>,
    back: Option<PathBuf>,
    tmpfs: Option<bool>,
    pids: Option<Vec<usize>>,
    gids: Option<Vec<usize>>,
    uids: Option<Vec<usize>>,
    ex_paths: Option<Vec<PathBuf>>,
}

impl CfgFile {
    pub fn load() -> CfgFile {
        const PKG_NAME: &str = env!("CARGO_PKG_NAME");

        info!("trying config with name {}", PKG_NAME);
        if let Some(bdirs) = directories::ProjectDirs::from("com", "meh", PKG_NAME) {
            let cfg_dir = bdirs.config_dir();

            if let Some(cfg) = fs::read_to_string(cfg_dir.join("config.yaml"))
                .ok()
                .and_then(|cfg| serde_yaml::from_str::<CfgFile>(&cfg).ok())
            {
                return cfg;
            }
        }

        info!("using default config");
        CfgFile::default()
    }
}

// TODO -- det_srm_home is called rather too many times -- maybe we should cache values...
pub struct CfgImpl {
    file: CfgFile,
    mount_util: MountUtils,
}

impl CfgImpl {
    pub fn new() -> CfgImpl {
        let mut mutils = MountUtils::new();
        mutils.refresh();

        CfgImpl {
            file: CfgFile::load(),
            mount_util: mutils,
        }
    }

    fn can_access_dir<P: AsRef<Path>>(p: P) -> bool {
        fs::create_dir_all(&p).is_ok() && tempfile_in(p).is_ok()
    }

    fn can_access_file<P: AsRef<Path>>(p: P) -> bool {
        let p = p.as_ref();
        let mut ok = true;
        if let Some(parent) = p.parent() {
            ok &= fs::create_dir_all(parent).is_ok();
        }

        ok &= OpenOptions::new().write(true).create(true).open(p).is_ok();

        ok
    }

    fn srm_home(p: PathBuf) -> PathBuf {
        p.join(
            vec![".", env!("CARGO_PKG_NAME")]
                .iter()
                .fold(String::new(), |mut acc, el| {
                    acc.push_str(el);
                    acc
                }),
        )
    }

    fn meta_file(p: PathBuf) -> PathBuf {
        p.join("meta.yaml")
    }

    fn det_srm_home<P: AsRef<Path>>(&self, p: P) -> Result<PathBuf> {
        let ref_path = p.as_ref();
        debug!("trying to determine home project folder for {:?}", ref_path);

        if !ref_path.as_os_str().is_empty() {
            if let Some(pm) = &self.file.home {
                debug!("trying user-defined home {:?}", pm);
                if CfgImpl::can_access_dir(pm) {
                    return Ok(pm.to_path_buf());
                }
            }

            if let Ok(mp) = self.mount_util.get_mount_from_path(p) {
                let t = CfgImpl::srm_home(mp);
                debug!("trying mount point {:?}", t);
                if CfgImpl::can_access_dir(&t) {
                    return Ok(t);
                }
            }
        }

        // could not determine mountpoint or we don't have write permissions
        // fallback to XDG_DATA_HOME, $HOME
        if let Some(base_dirs) = BaseDirs::new() {
            let data_dir = CfgImpl::srm_home(base_dirs.data_dir().to_path_buf());
            debug!("trying data dir {:?}", data_dir);
            if CfgImpl::can_access_dir(&data_dir) {
                return Ok(data_dir);
            }

            let home_dir = CfgImpl::srm_home(base_dirs.home_dir().to_path_buf());
            debug!("trying home dir {:?}", home_dir);
            if CfgImpl::can_access_dir(&home_dir) {
                return Ok(home_dir);
            }
        }

        Err(Error::Missing)
    }

    fn det_meta<P: AsRef<Path>>(&self, p: P) -> Result<PathBuf> {
        if let Some(pm) = &self.file.meta {
            let meta = CfgImpl::meta_file(pm.to_path_buf());
            if CfgImpl::can_access_file(&meta) {
                debug!("using default meta path is: {:?}", meta);
                return Ok(meta);
            }
        }

        let meta = CfgImpl::meta_file(self.det_srm_home(p)?);
        debug!("trying meta file {:?}", meta);
        if CfgImpl::can_access_file(&meta) {
            return Ok(meta);
        }

        Err(Error::Missing)
    }

    fn det_back<P: AsRef<Path>>(&self, p: P) -> Result<PathBuf> {
        if let Some(pm) = &self.file.back {
            if CfgImpl::can_access_dir(pm) {
                debug!("default backup path is: {:?}", pm);
                return Ok(pm.to_path_buf());
            }
        }

        let home = self.det_srm_home(p)?;
        if CfgImpl::can_access_dir(&home) {
            return Ok(home);
        }

        Err(Error::Missing)
    }
}

impl Config for CfgImpl {
    fn count(&self) -> usize {
        self.file.count.unwrap_or(10)
    }

    fn back_path<P: AsRef<Path>>(&self, p: P) -> Result<PathBuf> {
        self.det_back(p)
    }

    fn meta_path<P: AsRef<Path>>(&self, p: P) -> Result<PathBuf> {
        self.det_meta(p)
    }

    fn get_all_meta_paths(&self) -> Vec<PathBuf> {
        let mut mps: Vec<PathBuf> = self
            .mount_util
            .get_mount_points()
            .into_iter()
            .map(|path| CfgImpl::meta_file(CfgImpl::srm_home(path)))
            .filter(|path| CfgImpl::can_access_file(path))
            .collect();

        if let Ok(home_meta) = self.det_srm_home("") {
            mps.push(CfgImpl::meta_file(home_meta));
        }

        mps
    }

    fn exclude_tmpfs(&self) -> bool {
        self.file.tmpfs.unwrap_or_default()
    }
}
