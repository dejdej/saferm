use std::{
    path::{Path, PathBuf},
    time::SystemTime,
};

pub mod error;

use error::Result;

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, Clone)]
pub struct Id {
    pub del_time: SystemTime,
    pub orig_path: PathBuf,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Meta {
    pub id: Id,
    pub back_path: PathBuf,

    // of calling process
    pub uid: usize,
    pub pid: usize,
    pub gid: usize,
    pub cmd: Option<String>,
}

impl Meta {
    pub fn load<P: AsRef<Path>>(p: P) -> Result<Vec<Meta>> {
        Ok(serde_yaml::from_str(&std::fs::read_to_string(p)?)?)
    }

    pub fn create_for<P: AsRef<Path>>(p: P) -> Result<Meta> {
        Ok(Meta {
            id: Id {
                del_time: SystemTime::now(),
                orig_path: p.as_ref().canonicalize()?,
            },
            uid: users::get_effective_uid() as usize,
            gid: users::get_effective_gid() as usize,
            pid: std::process::id() as usize,
            back_path: PathBuf::new(),
            cmd: None,
        })
    }
}
