use std::path::Path;

mod config;
pub mod error;

use self::{
    error::Result,
    metadata::{Id, Meta},
    removal::Removal,
};

mod metadata;
mod removal;
mod utils;

pub struct SafeRm {
    rem: removal::Rmv,
}

impl SafeRm {
    pub fn new() -> SafeRm {
        SafeRm {
            rem: removal::create_removal(),
        }
    }

    pub fn backup<P: AsRef<Path>>(&mut self, paths: &[P]) -> Vec<Result<()>> {
        self.rem.backup(paths).into_iter().map(|e| Ok(e?)).collect()
    }

    pub fn restore_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>> {
        self.rem
            .restore_by_id(ids)
            .into_iter()
            .map(|e| Ok(e?))
            .collect()
    }
    pub fn restore_by_handle(&mut self, ids: &[u64]) -> Vec<Result<()>> {
        self.rem
            .restore_by_handle(ids)
            .into_iter()
            .map(|e| Ok(e?))
            .collect()
    }
    pub fn erase_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>> {
        self.rem
            .erase_by_id(ids)
            .into_iter()
            .map(|e| Ok(e?))
            .collect()
    }
    pub fn erase_by_handle(&mut self, ids: &[u64]) -> Vec<Result<()>> {
        self.rem
            .erase_by_handle(ids)
            .into_iter()
            .map(|e| Ok(e?))
            .collect()
    }

    pub fn list_backups(&self) -> Result<Vec<Meta>> {
        Ok(self.rem.list_backups()?)
    }
}

impl Default for SafeRm {
    fn default() -> Self {
        Self::new()
    }
}
