use std::{
    collections::{hash_map::DefaultHasher, HashMap},
    hash::{Hash, Hasher},
    path::{Path, PathBuf},
};

use crate::srm::{
    config::{self, Cfg, Config},
    metadata::{Id, Meta},
    utils::path::MountUtils,
};

use super::{
    error::{Error, Result},
    Removal,
};

pub struct RemovalImpl {
    cfg: Cfg,
    handle_to_meta: HashMap<u64, PathBuf>,
    meta_path_to_metas: HashMap<PathBuf, HashMap<u64, Meta>>, // meta path -> file metadatas
    mnt_utils: MountUtils, // TODO -- we already have this in config. re-use it somehow
}

impl RemovalImpl {
    pub fn new() -> RemovalImpl {
        let cfg = config::get_config();
        let mut metas = HashMap::new();
        let mut handles = HashMap::new();

        cfg.get_all_meta_paths().into_iter().for_each(|mp| {
            let mut fm = HashMap::new();
            match Meta::load(&mp) {
                Ok(meta) => {
                    for m in meta {
                        let meta_hash = RemovalImpl::calc_hash(&m.id);
                        fm.insert(meta_hash, m);
                        handles.insert(meta_hash, mp.clone());
                    }
                    metas.insert(mp, fm);
                }
                Err(e) => {
                    info!("could not load path({:?}) err({:?})", mp, e);
                    metas.insert(mp, fm);
                }
            }
        });

        let mut mnt = MountUtils::new();
        mnt.refresh();

        RemovalImpl {
            cfg,
            handle_to_meta: handles,
            meta_path_to_metas: metas,
            mnt_utils: mnt,
        }
    }

    fn mv<P>(from: P, to: P) -> Result<()>
    where
        P: AsRef<Path>,
    {
        let from = from.as_ref();
        let to = to.as_ref();

        if from.is_dir() {
            fs_extra::move_items(&[from], &to, &fs_extra::dir::CopyOptions::default())
                .map(|_| Ok(()))?

        // if it's file we rename first
        } else {
            let renamed = from
                .parent()
                .ok_or(Error::Missing)?
                .join(to.file_name().ok_or(Error::Missing)?);

            std::fs::rename(from, &renamed)?;
            fs_extra::move_items(
                &[renamed],
                &to.parent().ok_or(Error::Missing)?,
                &fs_extra::dir::CopyOptions::default(),
            )
            .map(|_| Ok(()))?
        }
    }

    fn increment_backup_count(path: PathBuf, max_backup: usize) -> Result<PathBuf> {
        if !path.exists() {
            Ok(path)
        } else {
            let orig_name = path
                .file_name()
                .ok_or(Error::Missing)?
                .to_str()
                .ok_or(Error::Missing)?;

            for i in 0..max_backup {
                let new_name = format!("{}.{}", orig_name, i);
                let new_path = Path::new(&new_name);
                if !new_path.exists() {
                    return Ok(new_path.to_path_buf());
                }
            }

            Err(Error::Invalid)
        }
    }

    fn rm<P: AsRef<Path>>(p: P) -> Result<()> {
        info!("removing {:?}", p.as_ref());
        Ok(fs_extra::remove_items(&[p])?)
    }

    fn get_meta_path_from_handle(&self, id: u64) -> Result<&PathBuf> {
        self.handle_to_meta.get(&id).ok_or(Error::Missing)
    }

    fn get_metas_from_handle(&mut self, id: u64) -> Result<&mut HashMap<u64, Meta>> {
        let mount = self.get_meta_path_from_handle(id)?.to_owned();
        self.meta_path_to_metas.get_mut(&mount).ok_or_else(|| {
            error!(
                "could not find entry for path({:?}), handle({:?})",
                mount, id
            );
            Error::Missing
        })
    }

    fn get_meta_from_handle(&self, id: u64) -> Result<&Meta> {
        let mount = self.get_meta_path_from_handle(id)?;
        self.meta_path_to_metas
            .get(mount)
            .ok_or_else(|| {
                error!("could not find entry for path({:?}), id({:?})", mount, id);
                Error::Missing
            })?
            .get(&id)
            .ok_or_else(|| {
                warn!("no backup file matches {:?}", id);
                Error::Missing
            })
    }

    fn handle_tmpfs<P: AsRef<Path>>(&self, p: P) -> Result<bool> {
        let p = p.as_ref();
        if self.cfg.exclude_tmpfs() && self.mnt_utils.is_tmpfs(p) {
            info!("erasing file placed on tmp file system");
            return RemovalImpl::rm(p).map(|_| true);
        }

        Ok(false)
    }

    fn handle_specialfs<P: AsRef<Path>>(&self, p: P) -> Result<bool> {
        let p = p.as_ref();
        if self.mnt_utils.is_special_mount(p) && !self.mnt_utils.is_tmpfs(p) {
            // this is shit -- TODO
            warn!("removing special file system path");
            return RemovalImpl::rm(p).map(|_| true);
        }
        Ok(false)
    }

    fn calc_hash<H: Hash>(h: &H) -> u64 {
        let mut hasher = DefaultHasher::new();
        h.hash(&mut hasher);
        hasher.finish()
    }

    fn erase_single_id(&mut self, id: u64) -> Result<()> {
        debug!("finding meta entry for {:?}", id);
        let meta = self.get_meta_from_handle(id)?;

        debug!("removing backup for {:?}", id);
        // find id if exists and remove
        RemovalImpl::rm(&meta.back_path)?;

        // update meta
        debug!("updating local metadata");
        let metas = self.get_metas_from_handle(id)?;
        metas.remove(&id);

        debug!("writing metas back");
        let updated_metas_yaml = serde_yaml::to_string(&metas)?;

        // write back
        let metas_path = self.get_meta_path_from_handle(id)?;
        //let metas_path = self.cfg.det_meta(&id.orig_path)?;
        std::fs::write(metas_path, updated_metas_yaml)?;
        Ok(())
    }

    fn restore_single_id(&mut self, id: u64) -> Result<()> {
        debug!("find meta entry for {:?}", id);

        let meta = self.get_meta_from_handle(id)?;

        debug!(
            "moving backup({:?}) to orig({:?})",
            meta.back_path, meta.id.orig_path
        );

        if meta.id.orig_path.exists() {
            warn!("restore path already exists, skipping...");
            return Err(Error::Exists);
        }

        RemovalImpl::mv(&meta.back_path, &meta.id.orig_path)?;

        let metas = self.get_metas_from_handle(id)?;
        debug!("update local metadata");
        metas.remove(&id);

        // TODO -- theoretically if these two fail we should revert the move....
        debug!("writing new metadatas");
        let updated_metas_yaml = serde_yaml::to_string(&metas)?;

        // write back
        let metas_path = self.get_meta_path_from_handle(id)?;
        std::fs::write(metas_path, updated_metas_yaml)?;

        Ok(())
    }
}

impl Default for RemovalImpl {
    fn default() -> Self {
        Self::new()
    }
}

impl Removal for RemovalImpl {
    fn backup<P: AsRef<std::path::Path>>(&mut self, paths: &[P]) -> Vec<Result<()>> {
        debug!("creating backups");
        paths
            .iter()
            .map(|p| {
                let p = p.as_ref();
                if !p.exists() {
                    info!("{:?} does not exist", p);
                    return Err(Error::Missing);
                }

                let can = if MountUtils::is_normal_file(p) {
                    p.to_path_buf()
                } else {
                    p.canonicalize()?
                };

                let p = &can;

                if self.handle_tmpfs(p)? {
                    return Ok(());
                }

                if self.handle_specialfs(p)? {
                    return Ok(());
                }

                // get meta location
                let meta_p = self.cfg.meta_path(p)?;

                let max_backup_cnt = self.cfg.count();

                debug!("meta path for {:?} is {:?}", p, meta_p);

                let metas = &mut self
                    .meta_path_to_metas
                    .get_mut(&meta_p)
                    .ok_or(Error::Missing)?;

                let original_metas_yaml =
                    serde_yaml::to_string(&metas.values().collect::<Vec<&Meta>>())?;

                // create meta for path
                debug!("creating metadata");
                let mut meta = Meta::create_for(p)?;

                // we need to determine backup path
                // store backups in format srm_home/orig_file_path_hash
                meta.back_path = self.cfg.back_path(&meta.id.orig_path)?;

                let mut hasher = DefaultHasher::new();
                meta.id.orig_path.hash(&mut hasher);
                meta.back_path = meta.back_path.join(hasher.finish().to_string());

                //check if backups already exist and maximum count
                meta.back_path =
                    RemovalImpl::increment_backup_count(meta.back_path, max_backup_cnt)?;

                debug!("backup path for {:?} shall be {:?}", p, meta.back_path);

                debug!("updating metadata");
                // update meta

                let meta_handle = RemovalImpl::calc_hash(&meta.id);

                debug!("meta hash for {:?} is {:?}", meta.id, meta_handle);

                metas.insert(meta_handle.clone(), meta.clone());

                self.handle_to_meta.insert(meta_handle, meta_p.clone());

                // write meta back
                let updated_metas_yaml =
                    serde_yaml::to_string(&metas.values().collect::<Vec<&Meta>>())?;

                std::fs::write(&meta_p, updated_metas_yaml)?;

                // backup + permissions
                info!("debug {:?} to {:?}", p, meta.back_path);

                //METADATA HAS BEEN WRITTEN AT THIS POINT WE NEED TO REVERT ON ERRORS
                if let Err(e) = RemovalImpl::mv(p, &meta.back_path) {
                    error!("Move operation has failed ({:?}).reverting metadata", e);
                    std::fs::write(&meta_p, original_metas_yaml)?;
                }
                Ok(())
            })
            .collect()
    }

    fn restore_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>> {
        debug!("restoring items");
        ids.iter()
            .map(|id| self.restore_single_id(RemovalImpl::calc_hash(id)))
            .collect()
    }

    fn erase_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>> {
        debug!("erasing items");
        ids.iter()
            .map(|id| self.erase_single_id(RemovalImpl::calc_hash(id)))
            .collect()
    }

    fn restore_by_handle(&mut self, ids: &[u64]) -> Vec<Result<()>> {
        debug!("restoring items");
        ids.iter().map(|id| self.restore_single_id(*id)).collect()
    }

    fn erase_by_handle(&mut self, ids: &[u64]) -> Vec<Result<()>> {
        debug!("erasing items");
        ids.iter().map(|id| self.erase_single_id(*id)).collect()
    }

    fn list_backups(&self) -> Result<Vec<Meta>> {
        let mut metas = Vec::new();
        for meta_paths in self.meta_path_to_metas.values() {
            for meta in meta_paths.values() {
                metas.push(meta.clone());
            }
        }

        Ok(metas)
    }
}
