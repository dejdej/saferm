use crate::srm::{config, metadata};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Io(std::io::Error),
    Serde(serde_yaml::Error),
    FsExtra(fs_extra::error::Error),
    Metadata(metadata::error::Error),
    Config(config::error::Error),
    Missing,
    Exists,
    Invalid,
}

impl From<config::error::Error> for Error {
    fn from(e: config::error::Error) -> Self {
        Error::Config(e)
    }
}

impl From<metadata::error::Error> for Error {
    fn from(e: metadata::error::Error) -> Self {
        Error::Metadata(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Io(e)
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(e: serde_yaml::Error) -> Self {
        Error::Serde(e)
    }
}

impl From<fs_extra::error::Error> for Error {
    fn from(e: fs_extra::error::Error) -> Self {
        Error::FsExtra(e)
    }
}
