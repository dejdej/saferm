mod concrete;
pub mod error;

use super::metadata::{Id, Meta};
pub use concrete::RemovalImpl as Rmv;
use error::Result;
use std::path::Path;

pub fn create_removal() -> Rmv {
    Rmv::new()
}

pub trait Removal {
    fn backup<P: AsRef<Path>>(&mut self, paths: &[P]) -> Vec<Result<()>>;
    fn restore_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>>;
    fn erase_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>>;
    fn restore_by_handle(&mut self, ids: &[u64]) -> Vec<Result<()>>;
    fn erase_by_handle(&mut self, ids: &[u64]) -> Vec<Result<()>>;
    fn list_backups(&self) -> Result<Vec<Meta>>;
}
