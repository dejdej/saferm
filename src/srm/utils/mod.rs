pub mod error;

pub mod path {
    use super::error::{Error, Result};
    use std::{
        cmp::Reverse,
        collections::HashMap,
        path::{Path, PathBuf},
    };

    use sysinfo::{DiskExt, System, SystemExt};

    #[derive(Debug, Clone)]
    struct ProcMounts {
        block: String,
        point: PathBuf,
        kind: String,
    }

    impl ProcMounts {
        pub fn is_tmpfs(&self) -> bool {
            self.kind.to_lowercase().eq("tmpfs") || self.kind.to_lowercase().eq("ramfs")
        }
    }

    pub struct MountUtils {
        sys: System,
        mounts: Option<Vec<PathBuf>>,
        specials: Option<HashMap<PathBuf, ProcMounts>>,
    }

    impl MountUtils {
        pub fn new() -> MountUtils {
            MountUtils {
                sys: System::new_all(),
                mounts: None,
                specials: None,
            }
        }

        pub fn is_normal_file<P: AsRef<Path>>(path: P) -> bool {
            let ref_path = path.as_ref();
            ref_path.is_file() || ref_path.is_dir()
        }

        pub fn get_mount_points(&self) -> Vec<PathBuf> {
            let mut dmp = self.get_disk_mount_points();
            let mut smp = self.get_special_mounts().keys().cloned().collect();

            dmp.append(&mut smp);

            dmp
        }

        pub fn get_disk_mount_points(&self) -> Vec<PathBuf> {
            self.mounts.clone().unwrap_or_else(|| {
                warn!("Most probably disk refresh has not been triggered!");
                Vec::new()
            })
        }

        // here paths must be canonicalized !
        pub fn get_mount_from_path<P: AsRef<Path>>(&self, path: P) -> Result<PathBuf> {
            let path = path.as_ref();
            let mut mps: Vec<PathBuf> = self.get_mount_points();
            mps.sort_by_key(|b| Reverse(b.as_os_str().len()));

            for p in mps {
                if path
                    .as_os_str()
                    .to_str()
                    .ok_or(Error::Missing)?
                    .contains(p.as_os_str().to_str().ok_or(Error::Missing)?)
                {
                    return Ok(p);
                }
            }

            Err(Error::Missing)
        }

        pub fn refresh(&mut self) {
            self.refresh_mount_points();
            self.refresh_specials();
        }

        fn refresh_mount_points(&mut self) {
            if self.mounts.is_none() {
                let mut mnts = Vec::new();
                for disk in self.sys.disks() {
                    mnts.push(disk.mount_point().to_path_buf());
                }
                self.mounts.replace(mnts);
            }
        }

        fn refresh_specials(&mut self) {
            if self.specials.is_none() {
                let mut mounts =
                    std::fs::read_to_string("/proc/mounts").map_or(HashMap::new(), |string| {
                        let mut _mounts = HashMap::new();

                        string.lines().for_each(|line| {
                            let elements: Vec<&str> = line.split_ascii_whitespace().collect();
                            if elements.len() < 2 {
                            } else {
                                _mounts.insert(
                                    PathBuf::from(elements.get(1).unwrap()),
                                    ProcMounts {
                                        block: elements.get(0).unwrap().to_string(),
                                        point: PathBuf::from(elements.get(1).unwrap()),
                                        kind: elements.get(2).unwrap().to_string(),
                                    },
                                );
                            }
                        });

                        _mounts
                    });

                for mnt in self.get_disk_mount_points() {
                    mounts.remove(&mnt);
                }
                self.specials.replace(mounts.clone());
            }
        }

        // basically if it's anything besides regular mount paths
        // read /proc/mounts on linux and compare with `list_mpaths`
        fn get_special_mounts(&self) -> HashMap<PathBuf, ProcMounts> {
            self.specials.clone().unwrap_or_else(|| {
                warn!("Most probably special refresh has not been triggered!");
                HashMap::new()
            })
        }

        #[cfg(not(target_os = "linux"))]
        pub fn is_special_mount<P: AsRef<Path>>(&self, _: &Path) -> bool {
            false
        }
        #[cfg(target_os = "linux")]
        pub fn is_special_mount<P: AsRef<Path>>(&self, pth: P) -> bool {
            let pth = pth.as_ref();
            let mounts = self.get_special_mounts();
            for k in mounts.keys() {
                if pth.starts_with(k) {
                    warn!("User passed special path {:?} ", pth);
                    return true;
                }
            }
            false
        }

        pub fn is_tmpfs<P: AsRef<Path>>(&self, pth: P) -> bool {
            let pth = pth.as_ref();
            let mounts = self.get_special_mounts();
            for (k, v) in mounts {
                if pth.starts_with(k) && v.is_tmpfs() {
                    warn!("User passed special path {:?}", pth);
                    return true;
                }
            }
            false
        }
    }
}
